<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_content']['fields']['imgFluid'] = [
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class' => 'w50'),
    'sql'                     => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['imgObjectFit'] = [
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array('object-contain', 'object-cover', 'object-fill', 'object-none', 'object-scale-down'),
    'eval'                    => ['includeBlankOption'=>true, 'tl_class'=>'w50'],
    'sql'                     => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['aspectRatio']['eval']['tl_class'] = 'w50 clr';

PaletteManipulator::create()

    // add legend
    ->addLegend('image_legend', 'source_legend', PaletteManipulator::POSITION_AFTER)

    // add fields
    ->addField('imgFluid', 'image_legend', PaletteManipulator::POSITION_APPEND)
    ->addField('aspectRatio', 'image_legend', PaletteManipulator::POSITION_APPEND)
    ->addField('imgObjectFit', 'image_legend', PaletteManipulator::POSITION_APPEND)

    // apply to (sub)palettes
    ->applyToPalette('image', 'tl_content')
    ->applyToSubpalette('addImage', 'tl_content')
;
