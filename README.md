# Contao-Image

## About

This extension for Contao Open Source CMS extended the default image content element and is customized to work together with [Conplate-Starterkit](https://gitlab.com/designerei-werbeagentur/conplate/conplate-starterkit).

## Functions

- Fluid display: Reset the predefined settings for responsive images
- Fit the whole image container via the css property `object-fit: cover`

Also it is extended with additional TailwindCSS classes and components to work together with [Conplate-Starterkit](https://gitlab.com/designerei-werbeagentur/conplate/conplate-starterkit). For a futher look, check out the styles within the file: [tailwind.config.js](https://gitlab.com/designerei-werbeagentur/conplate/conplate-starterkit/-/blob/master/tailwind.config.js).
