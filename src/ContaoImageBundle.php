<?php

namespace designerei\ContaoImageBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContaoImageBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
